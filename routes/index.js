/**
 * Manages the controllers &
 * routing to different web pages
 * 
 * @author(s): Luke Townsend, Mouni Krishna Atluri
 *
 * @requires express
 */

const express = require('express')
const LOG = require('../utils/logger.js')

LOG.debug('START routing')
const router = express.Router()

// Manage top-level request first
router.get('/', (req, res, next) => {
  LOG.debug('Request to /')
  res.render('index.ejs', { title: 'Express App' })
})

// Defer path requests to a particular controller
router.use('/orders', require('../controllers/orders.js'))
router.use('/product', require('../controllers/product.js'))
router.use('/orderlines', require('../controllers/orderline.js'))

LOG.debug('END routing')
module.exports = router
