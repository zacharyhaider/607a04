44-563 Group 06-06 - ECommerce App
About Us
Team Members

Customer - Chase Keller (Model, View, Controller)

Product - Mouni Krishna Atluri (Model, View, Controller)

Order - Luke Townsend (Model, View, Controller)

Order Line Item - Zachary Haider (Model, View, Controller)
Includes:
Customers
Products
Orders
OrderLine
Uses:
JSON
JavaScript
EJS
Instructions:
Run app locally
node app.js
From index.js choose an action
GET a view or GET JSON
Use available buttons to
Create, Edit, Delete, or View Details
Our Repository Link: https://bitbucket.org/zacharyhaider/607a04/src/master/
Our Heroku app Link: https://webapps-a04.herokuapp.com/