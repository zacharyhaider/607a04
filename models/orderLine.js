/*
Order Line Item Model
Describes each attribute in an order line item - one entry on a customer's order.
author: Zachary Haider s516936@nwmissouri.edu
*/

const mongoose = require('mongoose')

const OrderLineItemSchema = new mongoose.Schema({

    _id: { 
        type: Number, 
        required: true },

    orderID: {
        type: Number,
        required: true},

      lineNumber: {
        type: Number,
        required: true},
  
    productKey: {
        type: String,
        required: true},
  
    quantity: {
        type: Number,
        required: true, 
        default: 1}
})

module.exports = mongoose.model('OrderLineItem', OrderLineItemSchema)
