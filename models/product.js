

const mongoose = require('mongoose')

const ProductSchema = new mongoose.Schema({

  _id: { type: Number, required: true },
 
  productName: {
    type: String,
    required: true,
    unique: true,
    default: 'productname'
  },
  productDescription: {
    type: String,
    required: false,
    default: 'description'
  },
  productPrice: {
    type: Number,
    required: true,
    default: 4.99,
    min: 0.00,
    max: 99999
  }
})
module.exports = mongoose.model('product', ProductSchema)