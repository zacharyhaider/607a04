/**
 * Order Model
 * 
 * @author Luke Townsend
 */

 const mongoose = require('mongoose')

 const OrderSchema = new mongoose.Schema ({

    _id: { type: Number, required: true},

    datePlaced: {
        type: Date,
        required: true,
        default: Date.now()
    },

    dateShipped: {
        type: Date,
        required: false
    },

    email: {
        type: String,
        required: true
    },

    paymentType: {
        type: String,
        enum: ['not selected yet', 'card', 'cash', 'check'],
        required: true,
        default: 'not selected yet'
    },

    amountDue: {
        type: Number,
        required: false
    },

    paid: {
        type: Boolean,
        required: false
    },

 })
 module.exports = mongoose.model('Order', OrderSchema)