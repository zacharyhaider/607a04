//Author Luke Townsend  
const express = require('express')
const api = express.Router()
const Model = require('../models/order.js')
const LOG = require('../utils/logger.js')
const find = require('lodash.find')
const remove = require('lodash.remove')
const notfoundstring = 'orders'

// Respond with JSON

api.get('/findall', (req,res) => {
    res.setHeader('Content-Type', 'application/json')
    const data = req.app.locals.orders.query
    res.send(JSON.stringify(data))
})

api.get('/findone/:id', (req, res) => {
    res.setHeader('Content-Type', 'application/json')
    const id = parseInt(req.params.id, 10) // base 10
    const data = req.app.locals.orders.query
    const item = find(data, { _id: id })
    if (!item) { return res.end(notfoundstring) }
    res.send(JSON.stringify(item))
  })

  //Respond with views

api.get('/', (req, res) => {
    res.render('orders/index.ejs')
})

api.get('/create', (req, res) =>{
    LOG.info(`Handling GET /create${req}`)
    const item = new Model()
    LOG.debug(JSON.stringify(item))
      res.render('orders/create',
        {
            title: 'Create order',
            layout: 'layout.ejs',
            order: item
        })
})

api.get('/delete/:id', (req, res) =>{
    LOG.info(`Handling GET /delete/:id ${req}`)
    const id = parseInt(req.params.id, 10)
    const data = req.app.locals.orders.query
    const item = find(data, {_id: id})
    if(!item) {return res.end(notfoundstring)}
    LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
    return res.render('orders/delete.ejs',{
        title: 'Delete Order',
        layout: 'layout.ejs',
        order: item
    })
})

api.get('/details/:id', (req, res) =>{
    LOG.info(`Handling GET /details/:id ${req}`)
    const id = parseInt(req.params.id, 10) // base 10
    const data = req.app.locals.orders.query
    const item = find(data, { _id: id })
    if (!item) {return res.end(notfoundstring)}
    LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
    return res.render('orders/details.ejs',
    {
      title: 'Order Details',
      layout: 'layout.ejs',
      order: item
    })
})

api.get('/edit/:id', (req, res) =>{
    LOG.info(`Handling GET /edit/:id ${req}`)
    const id = parseInt(req.params.id, 10) // base 10
    const data = req.app.locals.orders.query
    const item = find(data, { _id: id })
    if (!item) { return res.end(notfoundstring) }
    LOG.info(`RETURNING VIEW FOR${JSON.stringify(item)}`)
    return res.render('orders/edit.ejs',
    {
      title: 'Order',
      layout: 'layout.ejs',
      order: item
    })
})

//Data modification requests

//POST new
api.post('/save', (req, res) => {
    LOG.info(`Handling POST ${req}`)
    LOG.debug(JSON.stringify(req.body))
    const data = req.app.locals.orders.query
    const item = new Model()
    LOG.info(`NEW ID ${req.body._id}`)
    item._id = parseInt(req.body._id, 10) // base 10
    item.datePlaced = req.body.datePlaced
    item.dateShipped = req.body.dateShipped
    item.email = req.body.email
    item.paymentType = req.body.paymentType
    item.amountDue = req.body.amountDue
    item.paid = req.body.paid
    data.push(item)
    LOG.info(`SAVING NEW order ${JSON.stringify(item)}`)
    return res.redirect('/orders')
})

//POST update
api.post('/save/:id', (req, res) => {
    LOG.info(`Handling SAVE request ${req}`)
    const id = parseInt(req.params.id, 10) // base 10
    //LOG.info(`Handling SAVING ID ${id}`)
    const data = req.app.locals.orders.query
    const item = find(data, { _id: id })
    if (!item) {return res.end(notfoundstring)}
    LOG.info(`ORIGINAL VALUES ${JSON.stringify(item)}`)
    LOG.info(`UPDATED VALUES: ${JSON.stringify(req.body)}`)
    item.datePlaced = req.body.datePlaced
    item.dateShipped = req.body.dateShipped
    item.email = req.body.email
    item.paymentType = req.body.paymentType
    item.amountDue = req.body.amountDue
    item.paid = req.body.paid
    LOG.info(`SAVING UPDATED order ${JSON.stringify(item)}`)
    return res.redirect('/orders')
})

//DELETE id
api.post('/delete/:id', (req, res) => {
    LOG.info(`Handling DELETE request ${req}`)
    const id = parseInt(req.params.id, 10) // base 10
    //LOG.info(`Handling REMOVING ID=${id}`)
    const data = req.app.locals.orders.query
    const item = find(data, { _id: id })
    if (!item) {
    return res.end(notfoundstring)
    }
    if (item.isActive) {
        item.isActive = false
        console.log(`Deacctivated item ${JSON.stringify(item)}`)
    } else {
        const item = remove(data, { _id: id })
        console.log(`Permanently deleted item ${JSON.stringify(item)}`)
    }
    return res.redirect('/orders')
})

module.exports = api