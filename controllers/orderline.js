//Author Zachary Haider
const express = require('express')
const api = express.Router()
const Model = require('../models/orderLine.js')
const LOG = require('../utils/logger.js')
const find = require('lodash.find')
const remove = require('lodash.remove')
const notfoundstring = 'orderline'


api.get('/findallorderline', (req,res) => {
    res.setHeader('Content-Type', 'application/json')
    const data = req.app.locals.orderline.query
    res.send(JSON.stringify(data))})

api.get('/findoneorderline/:id', (req, res) => {
    res.setHeader('Content-Type', 'application/json')
    const id = parseInt(req.params.id, 10) // base 10
    const data = req.app.locals.orderline.query
    const item = find(data, { _id: id })
    if (!item) { return res.end(notfoundstring) }
    res.send(JSON.stringify(item))})


api.get('/', (req, res) => {
    res.render('orderline/index.ejs')
})

api.get('/createorderline', (req, res) =>{
    LOG.info(`Handling GET /create${req}`)
    const item = new Model()
    LOG.debug(JSON.stringify(item))
      res.render('orderline/createorderline',
        {
            title: 'Create order Line object',
            layout: 'layout.ejs',
            orderline: item
        })
})

api.get('/deleteorderline/:id', (req, res) =>{
  LOG.info(`Handling GET /delete/:id ${req}`)
  const id = parseInt(req.params.id, 10) // base 10
  const data = req.app.locals.puppies.query
  const item = find(data, { _id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
  return res.render('orderline/delete.ejs',
    {
      title: 'Delete order Line object',
      layout: 'layout.ejs',
      orderline: item
    })
})

api.get('/detailorderline/:id', (req, res) =>{
    res.render('orderline/details.ejs')
})

api.get('/editorderline/:id', (req, res) =>{
    res.render('orderline/edit.ejs')
})

//Data modification requests

//POST new
api.post('/saveorderline', (req, res) => {
    return res.redirect('orderline/index.ejs')
})

//POST update
api.post('/saveorderline/:id', (req, res) => {
    return res.redirect('orderline/index.ejs')
})

//DELETE id
api.post('/deleteorderline/:id', (req, res) => {
    return res.redirect('orderline/index.ejs')
})

module.exports = api
